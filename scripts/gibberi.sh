#! /usr/bin/env bash

# Gibberish printer
# Selects n words randomly from the system dictionary and prints them out, where n is the number
# supplied as arg1 on the command line.
#

for i in $(cat /usr/share/dict/words); do echo "$RANDOM $i"; done | sort | sed -E 's|[0-9]+ (.+)|\1|' | head -$1 | tr "\\n" " " && echo
