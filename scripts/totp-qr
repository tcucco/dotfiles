#! /usr/bin/env python3
"""Creates a TOTP QR code on the command line"""

import argparse
import urllib.parse

import qrcode


def main():
    """Main entry point for program"""
    args = parse_args()
    url = build_url(args.code_type, args.issuer, args.account_name, args.secret)
    code = qrcode.QRCode()
    code.add_data(url)
    code.print_ascii()


def parse_args():
    """Parses command line args"""
    # https://github.com/google/google-authenticator/wiki/Key-Uri-Format
    parser = argparse.ArgumentParser(description="Generate TOTP QR Codes")
    parser.add_argument("-t", "--code-type", type=str, default="totp")
    parser.add_argument("-i", "--issuer", type=str, required=True)
    parser.add_argument("-a", "--account-name", type=str, required=True)
    parser.add_argument("-s", "--secret", type=str, required=True)
    return parser.parse_args()


def build_url(code_type: str, issuer: str, account_name: str, secret: str) -> str:
    """Builds the URL for the QR Code"""
    issuer_q = urllib.parse.quote(issuer)
    account_name_q = urllib.parse.quote(account_name)
    secret_q = secret.replace(" ", "")
    label = f"{issuer_q}:{account_name_q}"
    return f"otpauth://{code_type}/{label}?secret={secret_q}&issuer={issuer_q}"


if __name__ == "__main__":
    main()
