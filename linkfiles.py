#! /usr/bin/env python3
"""Script for linking and unlinking dotfiles from this repo."""

import argparse
from pathlib import Path
from typing import Any, Dict, Iterable, Tuple

DIRNAME = Path(__file__).parent


def main(args: Dict[str, Any]) -> None:
    """Main entrypoint for this script."""
    for file_directory in args["file_directory"]:
        link_files(file_directory, args["index_filename"], args["unlink_only"])


def parse_args() -> dict:
    """Parses command-line args"""
    apar = argparse.ArgumentParser(description="Link files to home directory")
    apar.add_argument("-d", "--file-directory", default=["rcfiles"], action="append")
    apar.add_argument("-f", "--index-filename", default="index.txt")
    apar.add_argument("-u", "--unlink-only", default=False, action="store_true")
    return apar.parse_args().__dict__


def link_files(file_directory: Path, index_filename: str, unlink_only: bool) -> None:
    """Links files from a repo directory to the home directory."""
    files_dir = DIRNAME / file_directory
    pairs = parse_pairs(files_dir, index_filename)

    for src, dest in pairs:
        dest_dirname = dest.parent

        if not dest_dirname.exists():
            dest_dirname.mkdir(parents=True)

        if dest.exists() or dest.is_symlink():
            dest.unlink()

        if not unlink_only:
            dest.symlink_to(src)


def parse_pairs(files_dir: Path, index_filename: str) -> Iterable[Tuple[Path, Path]]:
    """Pairse src/dest pairs from an index file."""
    index_filepath = files_dir / index_filename

    with open(index_filepath, "r") as rfile:
        for line in rfile:
            # Skip empty lines and comment lines
            if not line.strip() or line[0] == "#":
                continue

            (src_stem, dest_stem) = line.strip().split()

            src = files_dir / src_stem
            dest = Path.home() / dest_stem

            yield (src, dest)


if __name__ == "__main__":
    main(parse_args())
