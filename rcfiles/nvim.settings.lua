-- Map the leader character
vim.g.mapleader = ","

-- Allow edited buffers to be unfocused
vim.opt.hidden = true

-- Show line numbers. This also makes it so relativenumber shows the real line
-- number for the current line, rather than it's relative value (0)
vim.opt.number = true

-- Turn on relative line numbers.
vim.opt.relativenumber = true

-- Always show the status line
vim.opt.laststatus = 2

-- Highlight the line that contains the cursor
vim.opt.cursorline = true

-- Always show the sign column, this way text doesn't jump around
vim.opt.signcolumn = "yes"

-- Don't wrap text, preferring side-scroll
vim.opt.wrap = false

-- Do not use swapfiles
vim.opt.swapfile = false

-- Specifically define where to search for autocomplete. See :help cpt
vim.opt.complete = {'.', 'w', 'b', 'u', 't'}

-- Turn off spell check
-- TODO: I want to enable spelling, but only in comments, and for it to not
-- highlight code identifiers. Still working on that.
vim.opt.spell = false

-- Ignore misspellings inside ``. This lets us put code identifiers in comments
-- and not get spelling errors.
-- TODO: This isn't working properly
-- vim.cmd('syntax match EscapeSpelling +\\`[_a-zA-Z0-9-]\\+\\`+ contains=@NoSpell')

-- Allow backspacing over EOL, start, etc.
vim.opt.backspace = {'indent', 'eol', 'start'}

-- Searching
-- Highlight searches
vim.opt.hlsearch = true
-- Ignore case in searches
vim.opt.ignorecase = true
-- Don't ignore case if there is a upper case characters in the search string
vim.opt.smartcase = true
-- Highlight matching [{()}]
vim.opt.showmatch = true

-- Splits
-- Open vertical splits to the right
vim.opt.splitright = true
-- Open horizontal splits to the bottom
vim.opt.splitbelow = true

-- Indentation
-- Automatically determine indent level of new lines
vim.opt.autoindent = true
-- Number of spaces to use for each level of autoindent
vim.opt.shiftwidth = 2
-- Number of visual spaces per tab
vim.opt.tabstop = 2
-- Number of spaces in tab when editing
vim.opt.softtabstop = 2
-- Turn tabs into spaces
vim.opt.expandtab = true

-- Whitespace Display
-- Do not show whitespace characters by default
vim.opt.list = false
-- Define characters for when whitespace is turned on
vim.opt.listchars = {
  tab      = "»-", -- Chars to disdplay for tab
  trail    = "~",  -- Chars to display for trailing whitespace
  extends  = ">",  -- Chars to display to indicate horizontal scroll
  precedes = "<",  -- Chars to indicate wrapping
  nbsp     = "•",  -- Chars to display for non-breaking space
  space    = "•",  -- Chars to display for spaces
}

-- Folding
-- Turn on folding
vim.opt.foldenable = true
-- Do folding based on indentation level by default
vim.opt.foldmethod = "indent"
-- Open most folds by default
vim.opt.foldlevelstart = 10
-- 10 nested fold max
vim.opt.foldnestmax = 10

-- netrw
-- Hide the netrw banner
vim.g.netrw_banner = 0

-- Customize the buffer settings for netrw:
-- - show relative line numbers so it's easy to jump to the file we want
-- - hide the color column
vim.g.netrw_bufsettings = "noma nomod nu nobl nowrap ro rnu cc=0"
