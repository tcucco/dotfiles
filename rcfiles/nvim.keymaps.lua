function map(mode, shortcut, command)
  vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = false })
end

function nmap(shortcut, command)
  map('n', shortcut, command)
end

function imap(shortcut, command)
  map('i', shortcut, command)
end

-- Pressing C-p opens up FZF for fuzzy finding
nmap('<C-p>', ':Files<CR>')

-- Remove the C-w prefix for changing between splits
nmap('<C-h>', '<C-w><C-h>')
nmap('<C-l>', '<C-w><C-l>')
nmap('<C-j>', '<C-w><C-j>')
nmap('<C-k>', '<C-w><C-k>')

-- Allow changing between splits from insert mode, and without c-w prefix
imap('<C-h>', '<Esc><C-w><C-h>')
imap('<C-l>', '<Esc><C-w><C-l>')
imap('<C-j>', '<Esc><C-w><C-j>')
imap('<C-k>', '<Esc><C-w><C-k>')

-- Splits
nmap('<Leader>v', ':vsplit<CR>')
nmap('<Leader>s', ':split<CR>')

-- Remove search highlighting
nmap('<Leader>n', ':nohlsearch<CR>')

-- Search Mappings
-- Rg for the word under the cursor
nmap('K', '"zyiw:Rg <C-r>z')
-- Rg for the word under the cursor exactly
nmap('<Leader>fe', '"zyiw:Rg \\b<C-r>z\\b<CR>')
-- Rg for the word under the cursor as function usage
nmap('<Leader>ff', '"zyiw:Rg \\b<C-r>z\\(<CR>')
-- Rg for the word under the cursor as an HTML / React component
nmap('<Leader>fc', '"zyiw:Rg <<C-r>z\\b<CR>')

-- Search in current file for git merge conflict signs
nmap('<Leader>fg', '/<<<<\\|====\\|>>>><CR>')
-- Rg for git merge conflict signs
nmap('<Leader>fG', ':Rg <<<<\\|====\\|>>>><CR>')

-- Open netrw in the directory of the current buffer
nmap('-', ':Ex %:h<CR>')

-- Sorting methods
nmap('<Leader>Sl', 'vi]:sort<CR>') -- Sort lines in [], case-sensitive
nmap('<Leader>Sb', 'vi}:sort<CR>') -- Sort lines in {}, case-sensitive
nmap('<Leader>Sp', 'vi):sort<CR>') -- Sort lines in (), case-sensitive
nmap('<Leader>SP', 'v)k:sort<CR>') -- Sort lines in a para, case-sensitive
nmap('<Leader>Sil', 'vi]:sort i<CR>') -- Sort lines in [], case-insensitive
nmap('<Leader>Sib', 'vi}:sort i<CR>') -- Sort lines in {}, case-insensitive
nmap('<Leader>Sip', 'vi):sort i<CR>') -- Sort lines in (), case-insensitive
nmap('<Leader>SiP', 'v)k:sort i<CR>') -- Sort lines in a para, case-insensitive

-- Remove buffer without changing window layout
nmap('<Leader>bd', ':bp\\|bw#<CR>')

-- Show the syntax group for the character under the cursor:
nmap('<Leader>ss', [[:echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')<CR>]])
-- Shortcuts for ALE LSP
nmap('<Leader>d', ':ALEGoToDefinition<CR>')
nmap('<Leader>r', ':ALEFindReferences -quickfix<CR>:copen<CR>')
nmap('<Leader>h', ':ALEHover<CR>')
nmap('<Leader>R', ':ALERename<CR>')

-- Shortcuts for ALE
nmap('[[f', ':let b:ale_fix_on_save=0<CR>')
nmap(']]f', ':let b:ale_fix_on_save=1<CR>')
