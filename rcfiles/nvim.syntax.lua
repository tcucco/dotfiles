vim.cmd.filetype('plugin', 'on')

-- Language-specific settings
vim.cmd.autocmd('FileType', 'css',             'setl', 'cc=80')
vim.cmd.autocmd('FileType', 'javascript',      'setl', 'fdm=syntax', 'cc=100')
vim.cmd.autocmd('FileTYpe', 'lua',             'setl', 'cc=80')
vim.cmd.autocmd('FileType', 'markdown',        'setl', 'cc=80', 'wrap')
vim.cmd.autocmd('FileType', 'python',          'setl', 'sw=4', 'sts=4', 'ts=4', 'et', 'cc=88')
vim.cmd.autocmd('FileType', 'svelte',          'setl', 'fdm=syntax', 'cc=100')
vim.cmd.autocmd('FileType', 'typescript',      'setl', 'fdm=syntax', 'cc=100')
vim.cmd.autocmd('FileType', 'typescriptreact', 'setl', 'fdm=syntax', 'cc=100')

-- Set .jsx and .tsx files to typescriptreact
vim.cmd.autocmd('BufNewFile,BufRead', '*.tsx,*.jsx', 'set', 'filetype=typescriptreact')

-- Set .mdx files to markdown
vim.cmd.autocmd('BufNewFile,BufRead', '*.mdx', 'set', 'filetype=markdown')

-- Below I'm commenting out old syntax stuff in favor of treesitter

-- Color Scheme
-- Turn on syntax highlighting
-- vim.cmd.syntax("on")

-- monotonic doesn't work with termguicolors. In neovim 0.10 termguicolors is
-- enabled automatically. I need to update monotonic to work with termguicolors
-- but in the meantime I'm just going to turn it off. (Maybe forever?)
-- vim.opt.termguicolors = false

-- Select the color scheme
vim.opt.background = "dark"
-- vim.cmd.colorscheme("monotonic")

require'nvim-treesitter.configs'.setup {
  ensure_installed = { "javascript", "typescript", "tsx", "python", "rust", "go", "c", "lua", "vim", "vimdoc", "query" },
  sync_install = false,
  auto_install = false,
  ignore_install = {},
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
}
