-- Always show the sign column, so it doesn't bounce in and out as things change
vim.g.ale_sign_columns_always = 1

-- Do not show errors inline with virtual text
vim.g.ale_virtualtext_cursor = 0

-- Do not run the linter upon opening a file (it can be slow)
vim.g.ale_lint_on_enter = 0

-- Do run the linter after leaving insert mode
vim.g.ale_lint_on_insert_leave = 1

-- Run the linter after text changes in normal mode
vim.g.ale_lint_on_text_changed = 'normal'

-- Run fixers on saving a file
vim.g.ale_fix_on_save = 1

-- Cache executable checks to speed things up
vim.g.ale_cache_executable_check_failures = 1

-- Only run linters asked for
vim.g.ale_linters_explicit = 1

-- Configuration ale completion
vim.g.ale_completion_enabled = 0

vim.g.ale_echo_msg_error_str = 'E'
vim.g.ale_echo_msg_warning_str = 'W'
-- vim.g.ale_echo_msg_format = '[%linter%] %s'

-- Use eslint_d when available
-- For some reason, eslint_d isn't working where eslint does. It's trying to
-- load typescript-eslint/utils/node_modules/semver which isn't present in a
-- project I'm working on because it's installed further up the tree
-- vim.g.ale_javascript_eslint_executable = 'eslint_d'
-- vim.g.ale_javascript_eslint_use_global = 1

-- Turn on TSS log to debug issues
-- vim.g.ale_command_wrapper = 'TSS_LOG = "-level verbose -file /tmp/tss.log" '

vim.g.ale_python_pylsp_executable = 'pyls'

vim.g.ale_linters = {
 javascript = {'tsserver', 'eslint'},
 javascriptreact = {'tsserver', 'eslint'},
 python = {'ruff', 'mypy', 'pylsp'},
 svelte = {'tsserver', 'stylelint', 'eslint'},
 typescript = {'tsserver', 'eslint'},
 typescriptreact = {'tsserver', 'eslint'},
}

vim.g.ale_fixers = {
  ["*"] = {'remove_trailing_lines', 'trim_whitespace'},
  css = {'prettier'},
  javascript = {'prettier', 'eslint'},
  javascriptreact = {'prettier', 'eslint'},
  json = {'prettier'},
  less = {'prettier'},
  markdown = {'prettier'},
  python = {'ruff', 'black'},
  svelte = {'stylelint', 'eslint'},
  typescript = {'prettier', 'eslint'},
  typescriptreact = {'prettier', 'eslint'},
}

-- Temporarily testing out getting docker to work
-- mypy isn't working right with shadow files unless I set TMPDIR outside the
-- process

-- Put ALE's TMPDIR in the project so the files will be present in docker
-- Doesn't seem to be working
-- let $TMPDIR = '/Users/treycucco/Projects/closeio'
--
-- vim.g.ale_python_mypy_executable = '/Users/treycucco/Projects/close-py-scripts/mypy'
-- vim.g.ale_python_mypy_use_global = 1
--
-- vim.g.ale_python_ruff_executable = '/Users/treycucco/Projects/close-py-scripts/ruff'
-- vim.g.ale_python_ruff_use_global = 1
--
-- vim.g.ale_python_black_executable = '/Users/treycucco/Projects/close-py-scripts/black'
-- vim.g.ale_python_black_use_global = 1
--
-- vim.g.ale_filename_mappings = {
-- \  'black': [['/Users/treycucco/Projects/closeio', '/home/closeio/closeio']],
-- \  'mypy': [['/Users/treycucco/Projects/closeio', '/home/closeio/closeio']],
-- \  'ruff': [['/Users/treycucco/Projects/closeio', '/home/closeio/closeio']],
-- \}
