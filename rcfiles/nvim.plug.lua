-- TODO: Turn this into lua?
vim.cmd([[
call plug#begin()

" Navigate outside vim to other tmux panes
Plug 'christoomey/vim-tmux-navigator'

" FZF is a fuzzy finder for vim
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'https://gitlab.com/tcucco/vim-bracket'

" This is my own plugin that customizes the tabline the way I like it
Plug 'https://gitlab.com/tcucco/vim-tabline'

" This is my own plugin that customizes the statusline the way I like it
Plug 'https://gitlab.com/tcucco/vim-statusline'

" My custom color scheme
Plug 'https://gitlab.com/tcucco/vim-monotonic'

" vim-fugitive gives me various ways of interacting with git directly from vim.
Plug 'tpope/vim-fugitive'

" vim-surround adds additional `<action>[i|a|...]<object>` commands
Plug 'tpope/vim-surround'

" ALE asynchronous lint engine -- run compliant fixers on current buffer
Plug 'dense-analysis/ale'

" Debugging in vim
" Plug 'puremourning/vimspector'

""" Syntax Plugins
" GraphQL syntax
Plug 'jparise/vim-graphql'

" Plug 'othree/html5.vim'
" Plug 'evanleck/vim-svelte', {'branch': 'main'}

" Plug 'github/copilot.vim'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

call plug#end()
]])
