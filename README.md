# Dotfiles
## My Dotfiles

This project contains various config and run control files. I keep it centralized under version control to make it very simple to add to new computers and share.

## Installation

Run the `linkfiles.py` script to setup symlinks in the appropriate places

Check out `./linkfiles.py -h` for additional options.
